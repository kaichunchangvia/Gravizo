![Alt text](http://g.gravizo.com/source?https%3A%2F%2Fgitlab.com%2Fkaichunchangvia%2FGravizo%2Fraw%2Fmaster%2FGravizo.uml)

<img src='http://g.gravizo.com/g?
@startuml;
[AA] <<static lib>>;
[BB] <<shared lib>>;
[CC] <<static lib>>;
;
node node1;
node node2 <<shared node>>;
database Production;
;
skinparam component {;
    backgroundColor<<static lib>> DarkKhaki;
    backgroundColor<<shared lib>> Green;
};
;
skinparam node {;
  borderColor Green;
  backgroundColor Yellow;
  backgroundColor<<shared node>> Magenta;
};
skinparam databaseBackgroundColor Aqua;
;
@enduml
'>

![Alt text](https://g.gravizo.com/g?
  @startuml;
  [AA] <<static lib>>;
  [BB] <<shared lib>>;
  [CC] <<static lib>>;
  ;
  node node1;
  node node2 <<shared node>>;
  database Production;
  ;
  skinparam component {;
      backgroundColor<<static lib>> DarkKhaki;
      backgroundColor<<shared lib>> Green;
  };
  ;
  skinparam node {;
  	borderColor Green;
  	backgroundColor Yellow;
  	backgroundColor<<shared node>> Magenta;
  };
  skinparam databaseBackgroundColor Aqua;
  ;
  @enduml
)